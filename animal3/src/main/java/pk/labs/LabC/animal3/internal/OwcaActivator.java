/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal3.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

/**
 *
 * @author st
 */
public class OwcaActivator implements BundleActivator {

    Animal owca=new Owca();
    @Override
    public void start(BundleContext bc) throws Exception {
        
        //bc.registerService(Owca.class.getName(), new Owca(), null);
        bc.registerService(Animal.class.getName(), owca, null);
        System.out.println("Przychodzi Owca");
        Logger.get().log(this,"startOwca");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        System.out.println("Odchodzi Owca");
        Logger.get().log(this,"stopOwca");
    }
    
}
