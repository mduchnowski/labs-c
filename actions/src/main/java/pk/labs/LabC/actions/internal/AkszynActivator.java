/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.actions.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * @author st
 */
public class AkszynActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        
        bc.registerService(Akszyn.class.getName(), new Akszyn(), null);
        bc.registerService(Akszyn2.class.getName(), new Akszyn2(), null);
        bc.registerService(Akszyn3.class.getName(), new Akszyn3(), null);
        System.out.println("Akszyn started");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        System.out.println("Aksyzn stop");
    }
    
}
