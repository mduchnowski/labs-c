/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabC.animal1.internal;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import pk.labs.LabC.contracts.Animal;
import pk.labs.LabC.logger.Logger;

/**
 *
 * @author st
 */
public class PiesActivator implements BundleActivator {

    Animal pies=new Pies();
    @Override
    public void start(BundleContext bc) throws Exception {
        //bc.registerService(Pies.class.getName(), new Pies(), null);
        bc.registerService(Animal.class.getName(), pies, null);
        System.out.println("Przychodzi Pies");
        Logger.get().log(this,"start");
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        System.out.println("Odchodzi Pies");
        Logger.get().log(this,"stop");
    }
    
}
